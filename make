#!/bin/sh

# stolen from ffmpeg configure like a pig

# Prevent locale nonsense from breaking basic text processing.
LC_ALL=C
export LC_ALL

major=0
minor=9
micro=30
slib_file_name=libharfbuzz.so.$major.$minor.$micro
slib_soname=libharfbuzz.so.$major

api_hdrs='
hb-blob.h
hb-buffer.h
hb-common.h
hb-coretext.h
hb-deprecated.h
hb-face.h
hb-font.h
hb-ft.h
hb-glib.h
hb-gobject.h
hb-gobject-structs.h
hb-graphite2.h
hb.h
hb-icu.h
hb-ot-font.h
hb-ot.h
hb-ot-layout.h
hb-ot-shape.h
hb-ot-tag.h
hb-set.h
hb-shape.h
hb-shape-plan.h
hb-unicode.h
hb-uniscribe.h
'

srcs='
hb-blob.c
hb-buffer.c
hb-common.c
hb-face.c
hb-fallback-shape.c
hb-font.c
hb-ft.c
hb-glib.c
hb-open-file.c
hb-ot-layout.c
hb-ot-tag.c
hb-shape.c
hb-shape-plan.c
hb-shaper.c
hb-unicode.c
hb-utf-private.c
'

clean_do(){
    rm -Rf fake_root
    for src_file in $srcs
    do
      o_file=${src_file%.c}
      o_file=${o_file}.o
      rm -f "$o_file"
    done
    rm -f harfbuzz.pc hb-version.h
    exit 0
}

################################################################################

# find source path
if test -f make; then
    src_path=.
else
    src_path=$(cd $(dirname "$0"); pwd)
    echo "$src_path" | grep -q '[[:blank:]]' &&
        die "Out of tree builds are impossible with whitespace in source path."
    test -e "$src_path/config.h" &&
        die "Out of tree builds are impossible with config.h in source dir."
fi

is_in(){
    value=$1
    shift
    for var in $*; do
        [ $var = $value ] && return 0
    done
    return 1
}

append(){
    var=$1
    shift
    eval "$var=\"\$$var $*\""
}

die_unknown(){
    echo "Unknown option \"$1\"."
    echo "See $0 --help for available options."
    exit 1
}

set_default(){
    for opt; do
        eval : \${$opt:=\$${opt}_default}
    done
}

PATHS_LIST='
    prefix
    incdir
    libdir
'

CMDLINE_APPEND="
    extra_cflags
    extra_cxxflags
    host_cppflags
"

CMDLINE_SET="
    $PATHS_LIST
    slib_cc
    slib_ccld
    alib_ar
    disable-glib
"

#path defaults
prefix_default='/usr/local'
incdir_default='$prefix/include'
libdir_default='$prefix/lib'

#command lin set defaults
#for your information: harbuzz C API is shit when enabling c90 pedantic compilation
slib_cc_default='gcc -Wall -Wextra -Wno-missing-field-initializers -c -fPIC -O2 -std=c90'
slib_ccld_default="gcc -shared -Wl,-soname=$slib_soname"
alib_ar_default='ar rucs'
ln_s_default='ln -sf'
disable_glib_default=no

set_default $PATHS_LIST
set_default alib_ar slib_cc slib_ccld disable_glib

show_help(){
    cat <<EOF
Usage: make [options] [operations]

Operations: [default is to build the shared library]:
  clean                    clean build products


Options: [defaults in brackets after descriptions]

Help options:
  --help                   print this message

Features:
  --disable-glib           disable glib support [glib support is enabled]

Path options for pkg-config file:
  --prefix=PREFIX          install in PREFIX [$prefix_default]
  --libdir=DIR             install libs in DIR [$incdir_default]
  --incdir=DIR             install includes in DIR [$libdir_default]

Advanced options (experts only):
  --slib-cc=CC                 use C compiler command line CC for shared lib object[$slib_cc_default]
  --slib-ccld=CCLD             use linker command line LD for shared lib [$slib_ccld_default]
  --alib-ar=AR                 use archive command line AR to create the static library archive [$alib_ar_default]
EOF
  exit 0
}

for opt do
    optval="${opt#*=}"
    case "$opt" in
        clean) clean_do
        ;;
        --help|-h) show_help
        ;;
        --disable-glib) disable_glib=yes
        ;;
        *)
            optname="${opt%%=*}"
            optname="${optname#--}"
            optname=$(echo "$optname" | sed 's/-/_/g')
            if is_in $optname $CMDLINE_SET; then
                eval $optname='$optval'
            elif is_in $optname $CMDLINE_APPEND; then
                append $optname "$optval"
            else
                die_unknown $opt
            fi
        ;;
    esac
done

#-------------------------------------------------------------------------------
CPPFLAGS="-I./ -I$src_path		\
$(pkg-config --cflags freetype2)	\
"

if test x$disable_glib = xno; then
	CPPFLAGS="$CPPFLAGS -DHAVE_GLIB	\
	$(pkg-config --cflags glib-2.0)	\
	"
fi
#-------------------------------------------------------------------------------

#generate the version header file
sed -e "
s/@HB_VERSION_MAJOR@/$major/
s/@HB_VERSION_MINOR@/$minor/
s/@HB_VERSION_MICRO@/$micro/
s/@HB_VERSION@/$major.$minor.$micro/
" $src_path/hb-version.h.in >hb-version.h

#evaluate the final paths (depth of one)
eval "e_incdir=$incdir"
eval "e_libdir=$libdir"

#generate the pkg-config file
sed -e "
s:%prefix%:$prefix:
s:%exec_prefix%:$prefix:
s:%libdir%:$e_libdir:
s:%includedir%:$e_incdir:
s:%VERSION%:$major.$minor.$micro:
" $src_path/harfbuzz.pc.in >harfbuzz.pc

#compile source files, build a list of objet files
for src_file in $srcs
do
    obj=${src_file%.c}
    obj=${obj}.o
    echo "SLIB_CC $src_file"
    $slib_cc $CPPFLAGS -o "$obj" "$src_path/$src_file"
    objs="$obj $objs"
done

#link the shared lib
echo "SLIB_CCLD $slib_file_name"
echo $objs
mkdir -p "fake_root/$e_libdir"
$slib_ccld -o "fake_root/$e_libdir/$slib_file_name" $objs $(pkg-config --libs freetype2)

#install the other files in the fake root
ln -sf "$slib_file_name" "fake_root/$e_libdir/$slib_soname"
ln -sf "$slib_soname"  "fake_root/$e_libdir/libharfbuzz.so"

# create the library archive
echo "ALIB_AR libharfbuzz.a"
$alib_ar fake_root$e_libdir/libharfbuzz.a $objs

mkdir -p "fake_root/$e_libdir/pkgconfig"
cp -f harfbuzz.pc fake_root/$e_libdir/pkgconfig/harfbuzz.pc

mkdir -p "fake_root/$e_incdir/harfbuzz"
for hdr in $api_hdrs
do
     cp -f $src_path/$hdr "fake_root/$e_incdir/harfbuzz/$hdr"
     cp -f hb-version.h "fake_root/$e_incdir/harfbuzz/"
done
