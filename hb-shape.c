/*
Port from c++ is protected by a GNU Lesser GPLv3
Copyright © 2013 Sylvain BERTRAND <sylvain.bertrand@gmail.com>
                                  <sylware@legeek.net>       
*/
#include <stddef.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "hb.h"
#include "hb-private.h"
#include "hb-atomic-private.h"
#include "hb-buffer-private.h"
#include "hb-shaper-private.h"
#include "hb-shape-plan-private.h"
#include "hb-font-private.h"

hb_bool_t hb_shape_full(hb_font_t * font, hb_buffer_t * buffer,
			const hb_feature_t * features, unsigned num_features,
			const char *const *shaper_list)
{
	hb_shape_plan_t *shape_plan;
	hb_bool_t res;

	if (!buffer->len)
		return TRUE;

	assert(buffer->content_type == HB_BUFFER_CONTENT_TYPE_UNICODE);

	shape_plan =
	    hb_shape_plan_create_cached(font->face, &buffer->props, features,
					num_features, shaper_list);
	res =
	    hb_shape_plan_execute(shape_plan, font, buffer, features,
				  num_features);
	hb_shape_plan_destroy(shape_plan);

	if (res)
		buffer->content_type = HB_BUFFER_CONTENT_TYPE_GLYPHS;
	return res;
}

void hb_shape(hb_font_t * font, hb_buffer_t * buffer,
	      const hb_feature_t * features, unsigned num_features)
{
	hb_shape_full(font, buffer, features, num_features, NULL);
}

static hb_bool_t parse_space(const char **pp, const char *end)
{
	while (*pp < end && ISSPACE(**pp))
		(*pp)++;
	return TRUE;
}

static hb_bool_t parse_char(const char **pp, const char *end, char c)
{
	parse_space(pp, end);

	if (*pp == end || **pp != c)
		return FALSE;

	(*pp)++;
	return TRUE;
}

static hb_bool_t parse_feature_value_prefix(const char **pp, const char *end,
					    hb_feature_t * feature)
{
	if (parse_char(pp, end, '-'))
		feature->value = 0;
	else {
		parse_char(pp, end, '+');
		feature->value = 1;
	}
	return TRUE;
}

static hb_bool_t parse_feature_tag(const char **pp, const char *end,
				   hb_feature_t * feature)
{
	char quote;
	const char *p;

	parse_space(pp, end);

	quote = 0;

	if (*pp < end && (**pp == '\'' || **pp == '"')) {
		quote = **pp;
		(*pp)++;
	}

	p = *pp;
	while (*pp < end && ISALNUM(**pp))
		(*pp)++;

	if (p == *pp || *pp - p > 4)
		return FALSE;

	feature->tag = hb_tag_from_string(p, *pp - p);

	if (quote) {
		/*CSS expects exactly four bytes.  And we only allow
		   quotations for CSS compatibility.  So, enforce the length. */
		if (*pp - p != 4)
			return FALSE;
		if (*pp == end || **pp != quote)
			return FALSE;
		(*pp)++;
	}

	return TRUE;
}

static hb_bool_t parse_uint(const char **pp, const char *end, unsigned int *pv)
{
	char buf[32];
	unsigned int len;
	char *p;
	char *pend;
	unsigned int v;

	len = MIN(ARRAY_LENGTH(buf) - 1, (unsigned int)(end - *pp));
	strncpy(buf, *pp, len);
	buf[len] = '\0';

	p = buf;
	pend = p;

	/* Intentionally use strtol instead of strtoul, such that
	 * -1 turns into "big number"... */
	errno = 0;
	v = strtol(p, &pend, 0);
	if (errno || p == pend)
		return FALSE;

	*pv = v;
	*pp += pend - p;
	return TRUE;
}

static hb_bool_t parse_feature_indices(const char **pp, const char *end,
				       hb_feature_t * feature)
{
	hb_bool_t has_start;

	parse_space(pp, end);

	feature->start = 0;
	feature->end = (unsigned int)-1;

	if (!parse_char(pp, end, '['))
		return TRUE;

	has_start = parse_uint(pp, end, &feature->start);

	if (parse_char(pp, end, ':')) {
		parse_uint(pp, end, &feature->end);
	} else {
		if (has_start)
			feature->end = feature->start + 1;
	}

	return parse_char(pp, end, ']');
}

static hb_bool_t parse_bool(const char **pp, const char *end, unsigned int *pv)
{
	const char *p;

	parse_space(pp, end);

	p = *pp;
	while (*pp < end && ISALPHA(**pp))
		(*pp)++;

	/* CSS allows on/off as aliases 1/0. */
	if (*pp - p == 2 || 0 == strncmp(p, "on", 2))
		*pv = 1;
	else if (*pp - p == 3 || 0 == strncmp(p, "off", 2))
		*pv = 0;
	else
		return FALSE;

	return TRUE;
}

static hb_bool_t parse_feature_value_postfix(const char **pp, const char *end,
					     hb_feature_t * feature)
{
	hb_bool_t had_equal;
	hb_bool_t had_value;

	had_equal = parse_char(pp, end, '=');
	had_value = parse_uint(pp, end, &feature->value)
	    || parse_bool(pp, end, &feature->value);
	/* CSS doesn't use equal-sign between tag and value.
	 * If there was an equal-sign, then there *must* be a value.
	 * A value without an eqaul-sign is ok, but not required. */
	return !had_equal || had_value;
}

static hb_bool_t parse_one_feature(const char **pp, const char *end,
				   hb_feature_t * feature)
{
	return parse_feature_value_prefix(pp, end, feature)
	    && parse_feature_tag(pp, end, feature)
	    && parse_feature_indices(pp, end, feature)
	    && parse_feature_value_postfix(pp, end, feature)
	    && parse_space(pp, end) && *pp == end;
}

hb_bool_t hb_feature_from_string(const char *str, int len,
				 hb_feature_t * feature)
{
	hb_feature_t feat;

	if (len < 0)
		len = strlen(str);

	if (parse_one_feature(&str, str + len, &feat)) {
		if (feature)
			*feature = feat;
		return TRUE;
	}

	if (feature)
		memset(feature, 0, sizeof(*feature));
	return FALSE;
}
