#ifndef HB_PRIVATE_H
#define HB_PRIVATE_H
#if __GNUC__ >= 4
#define HB_UNUSED __attribute__((unused))
#else
#define HB_UNUSED
#endif

#ifdef __GNUC__
#define HB_INTERNAL __attribute__((__visibility__("hidden")))
#define INLINE __inline__
#else
#define HB_INTERNAL
#define INLINE
#endif

#define HB_DEBUG 0
#define FALSE	0
#define TRUE	1
#define REF_CNT_INVALID_VAL -1

#undef	MIN
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))

#undef ARRAY_LENGTH
#define ARRAY_LENGTH(array) (sizeof(array)/sizeof(array[0]))

static INLINE hb_bool_t hb_unsigned_int_mul_overflows(unsigned int count,
						      unsigned int size)
{
	return (size > 0) && (count >= ((unsigned int)-1) / size);
}

static INLINE hb_bool_t hb_codepoint_in_range(hb_codepoint_t u,
					      hb_codepoint_t lo,
					      hb_codepoint_t hi)
{
	if (((lo ^ hi) & lo) == 0 &&
	    ((lo ^ hi) & hi) == (lo ^ hi) && ((lo ^ hi) & ((lo ^ hi) + 1)) == 0)
		return (u & ~(lo ^ hi)) == lo;
	else
		return lo <= u && u <= hi;
}

static INLINE hb_bool_t hb_codepoint_in_ranges(hb_codepoint_t u,
					       hb_codepoint_t lo1,
					       hb_codepoint_t hi1,
					       hb_codepoint_t lo2,
					       hb_codepoint_t hi2,
					       hb_codepoint_t lo3,
					       hb_codepoint_t hi3)
{
	return hb_codepoint_in_range(u, lo1, hi1)
	    || hb_codepoint_in_range(u, lo2, hi2)
	    || hb_codepoint_in_range(u, lo3, hi3);
}

/* ASCII tag/character handling */

static INLINE hb_bool_t ISALPHA (unsigned char c)
{ return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'); }
static INLINE hb_bool_t ISALNUM (unsigned char c)
{ return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'); }
static INLINE hb_bool_t ISSPACE (unsigned char c)
{ return c == ' ' || c =='\f'|| c =='\n'|| c =='\r'|| c =='\t'|| c =='\v'; }
static INLINE unsigned char TOUPPER (unsigned char c)
{ return (c >= 'a' && c <= 'z') ? c - 'a' + 'A' : c; }
static INLINE unsigned char TOLOWER (unsigned char c)
{ return (c >= 'A' && c <= 'Z') ? c - 'A' + 'a' : c; }

#define HB_TAG_CHAR4(s) (HB_TAG(((const char *)s)[0], \
                                ((const char *)s)[1], \
                                ((const char *)s)[2], \
                                ((const char *)s)[3]))

/*
XXX:must be in sync with the "official" harfbuzz API 
HB_SEGMENT_PROPERTIES_DEFAULT in hb-buffer.h
*/
#define HB_SEGMENT_PROPERTIES_DEFAULT_INIT(x)	\
	(x).direction=HB_DIRECTION_INVALID;	\
	(x).script=HB_SCRIPT_INVALID;		\
	(x).language=HB_LANGUAGE_INVALID;	\
	(x).reserved1=NULL;			\
	(x).reserved2=NULL;
#endif
